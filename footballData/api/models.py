from django.db import models


class Team(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    name = models.CharField(max_length=30)
    tla = models.CharField(max_length=3)
    shortName = models.CharField(max_length=30)
    area = models.CharField(max_length=30)
    # email = models.EmailField(blank=True, unique=True)
    email = models.EmailField(blank=True, null=True)

    def __str__(self):
        return self.name


class Competition(models.Model):
    name = models.CharField(max_length=30)
    code = models.CharField(max_length=3)
    area = models.CharField(max_length=30)
    teams = models.ManyToManyField(Team)

    def __str__(self):
        return self.code


class Player(models.Model):
    name = models.CharField(max_length=30)
    position = models.CharField(max_length=30)
    dateOfBirth = models.DateTimeField()
    countryOfBirth = models.CharField(max_length=30)
    nationality = models.CharField(max_length=30)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def __str__(self):
        return self.name